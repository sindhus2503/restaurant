package com.jspider.restuarant.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jspider.worker.Entity.Duty;

public class RestuarantRepository {
	public void saveworkerDetails(Duty duty)
	{
		try {
	     Configuration cfg=new Configuration();
	     cfg.configure();
	     SessionFactory  sessionFactory =cfg.buildSessionFactory();
	     Session session=sessionFactory.openSession();
	     Transaction transaction=session.beginTransaction();
	     session.save(duty);
	     transaction.commit();
	}
		catch(HibernateException e) {
			e.printStackTrace();
		}
	}
}
