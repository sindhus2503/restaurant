package com.jspider.restuarant.Entity;

import java.io.Serializable;


@Entity
@Table(name=AppConstant.REST_INFO)
public class Restuarant implements Serializable{

	@Id
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private String type;
	
	@Column(name="averagePrice")
	private Long averagePrice;
	
	@Column(name="rating")
	private Long rating;
	
	@Column(name="city")
	private String city;
	
	@Column(name="contactNumber")
	private Long contactNumber;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(Long averagePrice) {
		this.averagePrice = averagePrice;
	}

	public Long getRating() {
		return rating;
	}

	public void setRating(Long rating) {
		this.rating = rating;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	
}
